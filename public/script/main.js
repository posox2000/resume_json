
var lastClickedNavItem=null;

$( document ).ready(function(){
	var navOffset = $('#navbar').height()+20; 
	$('.navbar a').click(function(event) {
		if($($(this).attr('href'))[0])
		{
		    event.preventDefault();
	    	$($(this).attr('href'))[0].scrollIntoView();
		    scrollBy(0, -navOffset);
		    $(this).parent().addClass('active');
		    lastClickedNavItem=$(this);
		}
	});

	$( window ).scroll(function() {
  		$('nav').children().children('li').removeClass('active');
  		if(lastClickedNavItem)
  		{
  			lastClickedNavItem.parent().addClass('active');
  			lastClickedNavItem=null;
  		}
	});
});

function prettyPrint() {
    var ugly = document.getElementById('myTextArea').value;
    var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);
    document.getElementById('myTextArea').value = pretty;
}