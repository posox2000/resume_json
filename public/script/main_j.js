
var lastClickedNavItem=null;
var CodeMirrorEditor=null;

$( document ).ready(function(){

	CodeMirrorEditor = CodeMirror.fromTextArea(myConfigTextArea, {
    	lineNumbers: true,
  		mode:  "javascript"
  	});
  	CodeMirrorEditor.setSize('100%', '500px');

  	var jqxhr = $.getJSON( filePath, function(data) { //"/script/resume.json"
  	   CodeMirrorEditor.doc.setValue(JSON.stringify(data, undefined, 4));
  	  //$('#myConfigTextArea').text(JSON.stringify(data, undefined, 4));
	  console.log( "success" );
	})
	  .done(function() {
	    console.log( "second success" );
	  })
	  .fail(function() {
	    console.log( "error load JSON config" );
	  });

});
function submitData(){
	CodeMirrorEditor.save();
	$('#myConfigTextArea').val(CodeMirrorEditor.doc.getValue());
	try{
		var one=JSON.parse($('#myConfigTextArea').val());
	}
	catch(err)
	{
		$.magnificPopup.open({
		items: {
			src: '<div class="white-popup"><p>Configuration is invalid.<br/>Please correct it or reload and start from the beginig.</p>'
			+'<button onclick="closePopup()" class="btn btn-warning">Close</button></div>', // can be a HTML string, jQuery object, or CSS selector
			type: 'inline'
			},
			closeBtnInside: false,
			callbacks: {
		      open: function() {
			      $.magnificPopup.instance.close = function() {
			        // Do whatever else you need to do here
			      
			        // Call the original close method to close the popup
			        $.magnificPopup.proto.close.call(this);
			        
			      };
			  }
		    }
		});	
		return false;
	}
	$('form').submit();
}

function viewResume(){
	$.magnificPopup.open({
		items: {
			src: '<div class="white-popup"><p>You can access CONF page here:<br/>'
				+'<a href="'+window.location.origin+'/config">'+window.location.origin+'/config</a>'
				+'<br/> SETUP button will be removed</p><button onclick="closePopup()" class="btn btn-success">OK</button></div>', // can be a HTML string, jQuery object, or CSS selector
			type: 'inline'
			},
			closeBtnInside: false,
			callbacks: {
		      open: function() {
			      $.magnificPopup.instance.close = function() {
			        // Do whatever else you need to do here
			      	window.location.href='/';
			        // Call the original close method to close the popup
			        $.magnificPopup.proto.close.call(this);
			        
			      };
			  }
		    }
		});	
}

function closePopup() {
  $.magnificPopup.close();
}

//just prityPrint JSON in <textarea></textarea>
function prettyPrint() {
    var ugly = document.getElementById('myTextArea').value;
    var obj = JSON.parse(ugly);
    var pretty = JSON.stringify(obj, undefined, 4);
    document.getElementById('myNewTextArea').value = pretty;
}
