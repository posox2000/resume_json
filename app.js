/*
 * Module dependencies
 */
var express = require('express')
  , stylus = require('stylus')
  , nib = require('nib')
  , bodyParser = require('body-parser')
  , fs = require('fs')
  , auth = require('basic-auth');;

var port = process.env.PORT || 8080;
var admin={'login':'root','pass':'password'};

var app = express()
function compile(str, path) {
  return stylus(str)
    .set('filename', path)
    .use(nib())
}
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.logger('dev'))
app.use(stylus.middleware(
  { src: __dirname + '/public'
  , compile: compile
  }
))
app.use(express.static(__dirname + '/public'))

// instruct the app to use the `bodyParser()` middleware for all routes
app.use(bodyParser.urlencoded({
  extended: true
}))

app.get('/', function (req, res) {
  var isConfigured='none'; 
  var confFilePath ='/public/resume.json';

  if(fileExists(__dirname + '/public/resume.json')){
    confFilePath ='resume.json';
    console.log('File exists - using saved CONF');
  } else {
    isConfigured ='block';    
    confFilePath ='/script/resume.json';
    console.log('NO File - using template CONF');
  }
  res.render('index',
    { title : 'Resume' ,isConfigured: isConfigured ,filePath : confFilePath}
  )
})

app.get('/config', function(req, res){

  //check credentials for config page
  var credentials = auth(req)
  if (!credentials || credentials.name !== admin.login || credentials.pass !== admin.pass) {
    res.statusCode = 401
    res.setHeader('WWW-Authenticate', 'Basic realm="Configuration"')
    res.end('Access denied')
  } else {
    //get default template if no config saved
    var confFilePath ='/public/resume.json';
    if(fileExists(__dirname + '/public/resume.json')){
      confFilePath ='resume.json';
      console.log('File exists - using saved CONF');
    } else {
      confFilePath ='/script/resume.json';
      console.log('NO File - using template CONF');
    }
    console.log (confFilePath);
    res.render('config_j',
      { title : 'Config', filePath : confFilePath, activateView:"hidden"}
    );
  }
});

// This route receives the posted form.
// As explained above, usage of 'body-parser' means
// that `req.body` will be filled in with the form elements
app.post('/config', function(req, res){

  var myConfigTextArea = req.body.myConfigTextArea;
  fs.writeFile(__dirname + '/public/resume.json',myConfigTextArea );
  res.render('config_j',
    { title : 'Saved' ,filePath : 'resume.json', activateView:'visible'}
  )

});

function fileExists(filePath)
{
    try
    {
        return fs.statSync(filePath).isFile();
    }
    catch (err)
    {
        return false;
    }
}

app.listen(port);
